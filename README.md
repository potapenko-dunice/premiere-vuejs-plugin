# Adobe Premiere Plugin - seed
* Adoptable css theme
* VueJS
* jQuery

## Build Setup

``` bash
# install dependencies
yarn install

# build for production
yarn run build

# make soft link from dist to CEP extension directory
[Location](https://github.com/Adobe-CEP/CEP-Resources/wiki/CEP-6-HTML-Extension-Cookbook-for-CC-2015#where-are-the-extensions)

ln -s <path_to_dist> <path_to_extensions>/name

# or sign it on Creative Cloud
```